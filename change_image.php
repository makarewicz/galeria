<?php
require_once('gallery.php');
require_once('common.php');
$gallery = new Gallery();
if (empty($_POST['img'])) {
    diplay_error('No image chosen');
    return;
}
$img_id = $_POST['img'];
$imgs = $gallery->get_image_details($img_id);
if (empty($imgs)) {
    display_error('Incorrect image id');
    return;
}
$img = $imgs[0];
session_start();
if (empty($_SESSION['username']) || $img['username'] != $_SESSION['username']) {
    display_error("You don't have authority to edit this image");
    return;
}
if (isset($_POST['change'])) {
    if (isset($_POST['desc'])) {
        $gallery->change_desc($img['img'], $_POST['desc']);
    }
} else if (isset($_POST['delete'])) {
    $gallery->delete_image($img['img']);
} else {
    display_error('Unknown command');
    return;
}
return_to_prev_page();
return;
?>
