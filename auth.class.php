<?php
require_once('common.php');
require_once('db_helper.php');

define('AUTH_SALT','iE!E@9%qz0IA:o1.TVEgZA/bty[+CKMaC*DS75)?QXYgiZ1Ln3+5|B5KbL)2kkp.');
define('PASS_DB_MAX_LEN', 60);

class Auth {
    function validate_alphanumeric_underscore($str)  {
        return preg_match('/^[a-zA-Z0-9_]+$/',$str);
    }

    function hash_password($password) {
        $hash = crypt($password, AUTH_SALT);
        return substr($hash, 0, PASS_DB_MAX_LEN);
    }

    function check_login_in_db($username, $password) {
        $mysqli = get_db_connection();
        $query = 'SELECT username, password FROM users WHERE username=? AND password=?';
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $stmt->bind_result($user, $pass);
        $success = false;
        if ($stmt->fetch()) {
            $success = true;
        }
        $stmt->close();
        $mysqli->close();
        return $success;
    }

    function check_username_in_db($username) {
        $mysqli = get_db_connection();
        $query = 'SELECT username FROM users WHERE username=?';
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->bind_result($user);
        $success = false;
        if ($stmt->fetch()) {
            $success = true;
        }
        $stmt->close();
        $mysqli->close();
        return $success;
    }

    function register_login_in_db($username, $password) {
        $mysqli = get_db_connection();
        $query = 'INSERT INTO users (username, password) VALUES(?, ?)';
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $success = false;
        if ($stmt->affected_rows > 0) {
            $success = true;
        }
        $stmt->close();
        $mysqli->close();
        return $success;
    }

    function read_username_and_password() {
        if (empty($_POST['username'])) {
            display_error('Username is empty!');
            return false;
        }
        $username = trim($_POST['username']);

        if (!$this->validate_alphanumeric_underscore($username)) {
            display_error('Username can only contain a-z, A-Z, 0-9 and _');
            return false;
        }
        if (strlen($username) > 30) {
            display_error('Username cannot be longer than 30 characters');
            return false;
        }

        if (empty($_POST['password'])) {
            display_error('Password is empty!');
            return false;
        }

        $password = trim($_POST['password']);
        $password = $this->hash_password($password);

        return array($username, $password);
    }

    function login() {
        list($username, $password) = $this->read_username_and_password();
        if (empty($username)) {
            return false;
        }

        if (!$this->check_login_in_db($username,$password)) {
            display_error('Wrong username or password.');
            return false;
        }

        session_start();
        $_SESSION['username'] = $username;
        return_to_prev_page();
        return true;
    }

    function register() {
        list($username, $password) = $this->read_username_and_password();

        if (empty($username)) {
            return false;
        }

        if ($this->check_username_in_db($username)) {
            display_error('Login <b>'.$username.'</b> already in use.');
            return false;
        }

        if (!$this->register_login_in_db($username, $password)) {
            display_error('Uknown error during registration, try again');
            return false;
        }

        session_start();
        $_SESSION['username'] = $username;
        return_to_page('user.php?u='.$username);
        return true;
    }

    function logout() {
        session_start();
        session_destroy();
        return_to_prev_page();
        return true;
    }
}
?>
