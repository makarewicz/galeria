<?php
function return_to_prev_page() {
    header('Location: ' . $_SERVER['HTTP_REFERER'], false);
}

function return_to_page($page) {
    header('Location: ' . $page, false);
}

function display_error($err_msg) {
    session_start();
    $_SESSION['err_msg'] = $err_msg;
    return_to_prev_page();
    return;
}

function extend_smarty_in_session_vars($smarty) {
    session_start();
    if (isset($_SESSION['username'])) {
        $smarty->assign('username', $_SESSION['username']);
    }
    if (isset($_SESSION['err_msg'])) {
        $smarty->assign('err_msg', $_SESSION['err_msg']);
        unset($_SESSION['err_msg']);
    }
}
?>
