<?php
require_once('config.php');
function get_db_connection() {
    global $db_config;
    $mysqli = new mysqli($db_config['host'], $db_config['login'], 
        $db_config['password'], $db_config['database'], $db_config['port']);
    if ($mysqli->connect_error) {
        die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
    }
    return $mysqli;
}
?>
