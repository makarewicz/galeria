<?php
require_once('db_helper.php');
require_once('smarty/Smarty.class.php');
require_once('common.php');
require_once('config.php');
class Gallery {
    function get_results($stmt) {
        $stmt->execute();
        $result = array();
        $stmt->bind_result($filename, $desc, $username, $ts);
        while ($stmt->fetch()) {
            if (empty($desc)) {
                $desc = "No description...";
            }
            array_push($result, array(
                filename => $filename, desc => $desc, username => $username, ts => $ts));
        }
        $stmt->close();
        return $result;
    }

    function get_newest_images() {
        $mysqli = get_db_connection();
        $query = 'SELECT * FROM imgs ORDER BY created_ts DESC LIMIT 100'; 
        $stmt = $mysqli->prepare($query);
        return $this->get_results($stmt);
    }

    function get_newest_images_by_user($username) {
        $mysqli = get_db_connection();
        $query = 'SELECT * FROM imgs WHERE username=? ORDER BY created_ts DESC'; 
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('s', $username);
        return $this->get_results($stmt);
    }

    function get_image_details($filename) {
        $mysqli = get_db_connection();
        $query = 'SELECT * FROM imgs WHERE filename=?';
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('s', $filename);
        return $this->get_results($stmt);
    }

    function change_desc($filename, $desc) {
        $mysqli = get_db_connection();
        $query = 'UPDATE imgs SET description=? WHERE filename=?';
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('ss', $desc, $filename);
        $stmt->execute();
        $stmt->close();
    }

    function delete_image($filename) {
        unlink(UPLOAD_FOLDER.'/'.$filename);
        unlink(THUMBNAIL_FOLDER.'/'.$filename);
        $mysqli = get_db_connection();
        $query = 'DELETE FROM imgs WHERE filename=?';
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('s', $filename);
        $stmt->execute();
        $stmt->close();
    }

    function generate_gallery($user=null) {
        $smarty = new Smarty();
        extend_smarty_in_session_vars($smarty);
        $gallery = new Gallery();
        if ($user) {
            $imgs = $this->get_newest_images_by_user($user);
        } else {
            $imgs = $this->get_newest_images();
        }
        $smarty->assign('images', $imgs); 
        $smarty->assign('UPLOAD_FOLDER', UPLOAD_FOLDER);
        $smarty->assign('THUMBNAIL_FOLDER', THUMBNAIL_FOLDER);
        $smarty->display("templates/gallery.html");
    }
}
?>
