<?php
require_once('config.php');
require_once('db_helper.php');
require_once('qqFileUploader.php');
function set_uploader() {
    $uploader = new qqFileUploader();
    $uploader->allowedExtensions = array('jpg', 'jpeg', 'gif', 'png', 'bmp');
    $uploader->inputName = 'qqfile';
    $uploader->chunksFolder = 'chunks';
    return $uploader;
}

function save_thumbnail($filename) {
    resize_save_jpeg(UPLOAD_FOLDER.'/'.$filename, THUMBNAIL_FOLDER.'/'.$filename, 300, 300, 75);
}

function resize_save_jpeg( $sourcefile, $targetfile, $target_image_width, $target_image_height, $quality )
{
    list( $source_image_width, $source_image_height, $source_image_type ) = getimagesize( $sourcefile );
    switch ( $source_image_type )
    {
    case IMAGETYPE_GIF:
        $source_gd_image = imagecreatefromgif( $sourcefile );
        break;
    case IMAGETYPE_JPEG:
        $source_gd_image = imagecreatefromjpeg( $sourcefile );
        break;
    case IMAGETYPE_PNG:
        $source_gd_image = imagecreatefrompng( $sourcefile );
        break;
    }

    if ( $source_gd_image === false ) {
        return false;
    }

    $source_aspect_ratio = $source_image_width / $source_image_height;
    $target_aspect_ratio = $target_image_width / $target_image_height;

    if ( $source_image_width <= $target_image_width && $source_image_height <= $target_image_height ) {
        $target_image_width = $source_image_width;
        $target_image_height = $source_image_height;
    } elseif ( $target_aspect_ratio > $source_aspect_ratio ) {
        $target_image_width = ( int ) ( $target_image_height * $source_aspect_ratio );
    } else {
        $target_image_height = ( int ) ( $target_image_width / $source_aspect_ratio );
    }

    $target_gd_image = imagecreatetruecolor( $target_image_width, $target_image_height );
    imagecopyresampled( $target_gd_image, $source_gd_image, 0, 0, 0, 0, $target_image_width,
        $target_image_height, $source_image_width, $source_image_height );
    imagejpeg( $target_gd_image, $targetfile, $quality );
    imagedestroy( $source_gd_image );
    imagedestroy( $target_gd_image );
    return true;
}

function save_img_to_db($filename) {
    $mysqli = get_db_connection();
    $query = 'INSERT INTO imgs (filename, username, description) VALUES(?,?,"")';
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('ss', $filename, $_SESSION['username']);
    $stmt->execute();
    $success = false;
    if ($stmt->affected_rows > 0) {
        $success = true;
    }
    $stmt->close();
    $mysqli->close();
    return $success;
}

session_start();
if (isset($_SESSION['username'])) {
    $uploader = set_uploader();
    $pathinfo = pathinfo($uploader->getName());
    $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
    $name = md5(rand()).'.'. $ext;
    // Basicly reads input and saves it to file with bunch of cool
    // ifs around broken cases.
    $result = $uploader->handleUpload(UPLOAD_FOLDER, $name);

    $result['uploadName'] = $uploader->getUploadName();
    save_thumbnail($uploader->getUploadName());
    save_img_to_db($uploader->getUploadName());
} else {
    $result = array('error' => 'User not logged in');
}

header('Content-Type: text/plain');
echo json_encode($result);
?>
